---
# vim:tw=100:ft=markdown
author: <white><small>Sander Apweiler (FZJ), Marcus Hardt (KIT), Uwe Jandt (DESY), Andreas Klotz (HZB)</small></white>
title: <white><br/><br/><small> Helmholtz-AAI </small></white>
date: <white>October 2021</white>
theme: marcus
parallaxBackgroundImage: images/helmholtz-bg-slide.png
title-slide-attributes:
    data-background-image: images/helmholtz-bg-head.png
slideNumber: \'c/t\'
preloadIframes: true
showNotes: false
mouseWheel: true
transition: none
backgroundTransition: none
---

## Outline
- Motivation
- Architecture
- Implementation
- Developments


# Motivation <br/> + <br/> Overview
## Historical records
- Helmholtz Data Federation (HDF) needed an AAI (back in 2017)
- Proof of Concept implementation of the [AARC Blueprint Architecture](https://aarc-community.org/architecture)
    - SP-IdP Proxy (in eduGain)
    - 4 Initial services (Nagios, OpenStack, dCache, WaTTS, ...)
    - OpenID Connect as a primary target
- Adaptation of the [AARC Policy Development Kit](https://aarc-community.org/policies/policy-development-kit)
    - Security + Trust
    - Policy Compatibility with large infrastructures (WLCG, LIGO, XSEDE, ELIXIR, ...)

<none class="fragment"data-fragment-index="2"> Then HIFIS started


## {data-background-image="images/motivation1b.png" data-background-size="fill"}
## {data-background-image="images/motivation2b.png" data-background-size="fill"}
## {data-background-image="images/motivation3b.png" data-background-size="fill"}


## Helmholtz AAI: Goals
- Users can access **many** federated services
    - with the **one account** of their Home Organisation
- Seamless access
    - Enable Services for users at Helmholtz Centres and Partners
    - Enable researchers and guests to access Services
    - Very general approach => Don't be limited by specific organisational structure
- Enable PIs to manage their own Virtual Organisations (VOs)
- Compatibility with the European Open Science Cloud (EOSC)
- Support for services beyond the browser
    - Delegation (Computing Jobs)
    - REST APIs
    - Shell access


## Relation to DFN-AAI
- Overlap only in the acronym "AAI"
- Helmholtz AAI is _one_ service inside DFN-AAI
    - it's an SP-IdP-proxy
- Users come in via 
    - **DFN-AAI**, eduGAIN, 
    - And others: ORCID, Github, Google
    - Even "Homeless Users" __could__ be supported


## {data-background-image="images/foederationen_und_bpa_mod.png" data-background-size="contain"}


# Architecture
## AARC
- [Authentication and Authorisation for Research Communities (AARC)](https://aarc-project.eu)
    - 25 Partners 
    - 4 Years
- Mission
    - Analyse **existing** Architectures
    - Analyse **existing** Policies
    - => Give recommendations 
        - 21 Final, ~15 more on the roadmap


## AARC Results
- [AARC Policy Development KIT](https://aarc-community.org/policies/policy-development-kit/)
    - Fundamental policy templates for
        - Operating an infrastructure
        - Handling Incident Response
        - Manage Members
        - Requirements on Authentication
        - Risk Assessment
        - Data Protection
        - Privacy Policy
        - Service Operation
        - Acceptable Use Policy

         **All policies designed to be GDPR compliant**

- [AARC Blueprint Architectures](https://aarc-community.org/architecture/) 
    - Introduction of the "proxy" component


## {data-background-image="images/bpa-final-glow.png" data-background-size="contain"}


## The **"proxy"** component
aka: "SP-IdP-Proxy"

- Scalability!
    - Stop every IdP needing to talk to every SP (`2800 * 1800`)
    - Reliable Attribute release 
        - (Different nations, different IdPs, each with different schema)
- Third party **authorisation**
    - *"Community Attribute Services"*
- Enforcement of authorisation
- Protocol Translation
    - SAML, OIDC, X.509
- Stackable


## Evolution of the BPA (~2019)
- Add differentiation:
    - Between **community** and **infrastructure**
    - Between "infrastructure run by a community" and "general e-Infrastructure"
    - Different types of Services
- Introduces the vocabulary used 
- Full Document: <small>[https://zenodo.org/record/3672785/files/AARC-G045-AARC_BPA_2019-Final.pdf](https://zenodo.org/record/3672785/files/AARC-G045-AARC_BPA_2019-Final.pdf)</small>

## {data-background-image="images/bpa2019.png" data-background-size="contain"}


# Implementation

## **Helmholtz-AAI** <br/>implements<br/> **AARC BPA**

## Technical implementation
- Software: `unity` (also used for Eudat's [b2access](https://b2access.eudat.eu))
    - Production: [https://login.helmholtz.de](https://login.helmholtz.de)
    - Development: [https://login-dev.helmholtz.de](https://login-dev.helmholtz.de)
- Self service Group Membership:
    - Principal Investigators can request a group
    - Manage their members


## Helmholtz-AAI Features
- Well documented at [https://aai.helmholtz.de](https://aai.helmholtz.de)
- Implements the [Policy Development Kit](https://aarc-community.org/policies/policy-development-kit)
- Follows AARC recommendations (a lot of the `G0XY` documents)
    - <=> To use specific schemas for attributes and their content
    - HIFIS is an (observing) member of AEGIS
- Focus on OIDC
    - OpenID is not OpenID Connect
    - OpenID connect is defined be the OpenID Foundation
    - The OpenID protocol is deprecated
- Three levels of authorisation
    - Community based
    - Home Organisation Based
    - Assurance Based


## Authorisation Management<br/>Based on **Community**
<div class="columns">
  <div class="column">
- **Virtual Organisation (VO)** approach
- Very similar: HPC compute projects
- **VO** Managers can administer community members
- Services can filter users by
    - VO Attributes
        - `climate -> ozone -> south-pole`
        - `cern -> cms -> admin`

 </div>
  <div class="column">
```json
  "eduperson_entitlement": [
    "urn:geant:helmholtz.de:group:Helmholtz-member",
    "urn:geant:helmholtz.de:group:HIFIS:Associates",
    "urn:geant:helmholtz.de:group:HIFIS:Core",
    "urn:geant:helmholtz.de:group:HIFIS",
    "urn:geant:helmholtz.de:group:IMK-TRO-EWCC",
    "urn:geant:helmholtz.de:group:KIT"
  ]
```
</div>
 </div>


## Authorisation Management<br/>Based on **Origin**
<div class="columns">
  <div class="column">
- **Home-IdP based** approach
- Home IdP can assert complementary information
- Services can filter users by
    - Home-Org asserted eligibility to use certain resources
    - Status: - Employee / Student / Guest

 </div>
  <div class="column">
```json
  "eduperson_entitlement": [
    "http://bwidm.de/entitlement/bwLSDF-SyncShare",
    "urn:mace:dir:entitlement:common-lib-terms",
  ]
```
</div>
 </div>


## Authorisation Management<br/>Based on **Assurance**
<div class="columns">
  <div class="column">
- Levels of Assurance: [REFEDS Assurance Framework](https://refeds.org/assurance)
    - Passport seen, Work-Contract available (Most academic Institutes)
    - Uniqueness of the identifier
    - Freshness of attributes
    - Verified Email Address (Social Media)
- Benefit
    - AAI can host "lesser-than-maximum" users
    - Scientists only need to upgrade their identity, if necessary to access service
    - Services can provide different levels of access

 </div>
  <div class="column">
```json
  "eduperson_assurance": [
    "https://refeds.org/assurance/profile/cappuccino",
    "https://refeds.org/assurance/ATP/ePA-1d",
    "https://refeds.org/assurance/ATP/ePA-1m",
    "https://refeds.org/assurance/IAP/local-enterprise",
    "https://refeds.org/assurance/IAP/low",
    "https://refeds.org/assurance/IAP/medium",
    "https://refeds.org/assurance/ID/eppn-unique-no-reassign",
    "https://refeds.org/assurance/ID/unique"
  ]
```
</div>
 </div>

## Information available at services
```json
{
    "body": {
        "aud": "oidc-agent-marcus2",
        "client_id": "oidc-agent-marcus2",
        "exp": 1635174663,
        "iat": 1635170663,
        "iss": "https://login.helmholtz.de/oauth2",
        "jti": "c8978ad3-0296-43a4-bad2-1e6045a767a4",
        "scope": "openid display_name sn email profile credentials eduperson_scoped_affiliation eduperson_entitlement eduperson_principal_name eduperson_unique_id eduperson_assurance",
        "sub": "6c611e2a-2c1c-487f-9948-c058a36c8f0e"
    },
    "header": {
        "alg": "RS256",
        "typ": "at+jwt"
    },
    "signature": "PO2KI0-BtyzT98avx3qYmJQzrDHvwkNYPrczoKn_V1udVuUAzoVCO7g9w2XhTIFWOV7mCr7J0edqx3MEuEhi8iq57UDJIUrJto6fw4M84OyxbTlNyjGz6aw8Xm3hqxCvLlKB8840h-58FtbwfuvKjyY5eCs3LnyY84Rjd-Fg3-fsRbIsozfDiVLO_WudOAgbbJx9OzsHcdjargxPt7fnMZzo5RCqgcHT4stEFK7AjYDOIjnB97kTQ0y1yRKLiNo1eSzoNgbdaJctH0GhuHZk1r-S1o4EK5r34kesOoopI9pFtpvwuyQctJFgc71CzSlEBWMz5eEiLzXnRaaqBvIo3g"
}

{
    "display_name": "Marcus Hardt",
    "eduperson_assurance": [
        "https://refeds.org/assurance/profile/cappuccino",
        "https://refeds.org/assurance/ATP/ePA-1d",
        "https://refeds.org/assurance/ATP/ePA-1m",
        "https://refeds.org/assurance/IAP/local-enterprise",
        "https://refeds.org/assurance/IAP/low",
        "https://refeds.org/assurance/IAP/medium",
        "https://refeds.org/assurance/ID/eppn-unique-no-reassign",
        "https://refeds.org/assurance/ID/unique"
    ],
    "eduperson_entitlement": [
        "urn:geant:h-df.de:group:HDF#login.helmholtz.de",
        "urn:geant:h-df.de:group:lsdf_admin#login.helmholtz.de",
        "urn:geant:h-df.de:group:m-team:feudal-developers#login.helmholtz.de",
        "urn:geant:h-df.de:group:m-team#login.helmholtz.de",
        "urn:geant:h-df.de:group:MyExampleColab#login.helmholtz.de",
        "urn:geant:h-df.de:group:wlcg-test#login.helmholtz.de",
        "urn:geant:helmholtz.de:group:Helmholtz-member#login.helmholtz.de",
        "urn:geant:helmholtz.de:group:HIFIS:Associates#login.helmholtz.de",
        "urn:geant:helmholtz.de:group:HIFIS:Core#login.helmholtz.de",
        "urn:geant:helmholtz.de:group:HIFIS#login.helmholtz.de",
        "urn:geant:helmholtz.de:group:IMK-TRO-EWCC#login.helmholtz.de",
        "urn:geant:helmholtz.de:group:KIT#login.helmholtz.de",
        "urn:mace:dir:entitlement:common-lib-terms",
        "http://bwidm.de/entitlement/bwLSDF-SyncShare"
    ],
    "eduperson_principal_name": "lo0018@kit.edu",
    "eduperson_scoped_affiliation": [
        "employee@kit.edu",
        "member@kit.edu"
    ],
    "eduperson_unique_id": "6c611e2a2c1c487f9948c058a36c8f0e@login.helmholtz-data-federation.de",
    "email": "marcus.hardt@kit.edu",
    "email_verified": true,
    "family_name": "Hardt",
    "given_name": "Marcus",
    "name": "Marcus Hardt",
    "preferred_username": "marcus",
    "sn": "Hardt",
    "ssh_key": "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAqA5FW6m3FbFhCOsRQBxKMRki5qJxoNhZdaeLXg6ym/ marcus@test2022\n",
    "sub": "6c611e2a-2c1c-487f-9948-c058a36c8f0e"
}
```


## Connected Services
- Multi Protocol:
    - Identities: SAML, OpenID Connect, X.509
    - Services: OpenID Connect, SAML
- Integrated Services:
    - Helmholtz Federated IT services (HIFIS, [hifis.net](https://hifis.net))
      - Drives development, documentation and service integration
      - [cloud.helmholtz.de/services](https://cloud.helmholtz.de/services)
  - Technially feasible: Rocketchat, Storage, Compute & more.
  - More pilot services at [documentation pages](https://hifis.net/doc/cloud-services/list-of-services/#pilot-services)
  - Exhaustive list: [aai.helmholtz.de/services](https://aai.helmholtz.de/services)


## AAI usage
<img src="images/aai-usage-plot.png" width=85%>



# AAI Developments in Helmholtz
## Helmholtz Cloud Agent
- [https://hifis.net/doc/service-integration/local-agent/](https://hifis.net/doc/service-integration/local-agent/)
- First (exemplary) use-case: Nubes
    - Enable DESY cloud portal (cloud.helmholtz.de)
    - To use (Nubes)[https://nubes.helmholtz-berlin.de](NextCloud) resources at HZB
    - Challenge: 
        - Exchange user provisioning information
        - Integrate local systems


## oidc-agent
- Goal: Support OIDC on end user computers
- Initial goal: Unix commandline (linux + mac)
    - Handle all the issues with different OIDC Providers
    - Adequate security features
        - All sensitive information on disk is encrypted 
        - Everything (sensitive) in RAM is obfuscated
        - Keep the user from stupid moves
    - Works just like ssh-agent
        - `oidc-agent, oidc-gen, oidc-add, oidc-token`
        - Including **agent forwarding** and **x-session integration**
    - Works well with many OIDC providers 
        - <bitsmall> Google, Eudat, eduTEAMS, EGI-Checkin, Elixir, Helmholtz-AAI, WLCG, Indigo IAM, KIT, Human Brain, ...</bitsmall>
- New goal: Support for GUI environments (windows + mac + linux)

## mytoken
- Mytokens are a new class of tokens
- Use case: Long running compute job<br/>
    - Longer than lifetime of Access Token

<div class="fragment"data-fragment-index="2">
<div class="columns">
  <div class="column">
  - **Mytoken Server**
      - Proxy for Refresh Tokens (RT)
      - Implemented as an extension of OIDC
  - User flow:
      1. Create mytoken (MT)
      2. Use MT to obtain
            - Access Tokens (AT)
            - Other mytokens</ul></ol> </ul>

<div class="fragment"data-fragment-index="3">
```json
[{"exp"        :1634300000,
  "nbf"        :1634400000,
  "geoip_allow":["DE"],
  "scope"      :"compute.create",
 },{
  "exp"        :1635300000,
  "nbf"        :1635400000,
  "geoip_allow":["DE", "FR", "NL"],
  "scope"      :"storage.write",
}]
```
</div>

  </div>
  <div class="column"><img class="plain" src="images/mytokenGeneralConcept.png" width=90%></div>
</div>
</div>


## ssh-oidc
- Enable **ssh** via **federated identity** (OIDC)
    - without recompiling OpenSSH
    - with a clear authorisation concept
- Solution:
    - PAM module
    - Mapping Daemon
    - Client Wrapper
- Available for Linux
    - Mac and Windows in development (Putty, maybe: MobaXterm)
- Test it at [https://ssh-oidc-demo.data.kit.edu](https://ssh-oidc-demo.data.kit.edu)

<!--## Developments-->
<!--- [Helmholtz Cloud Agent](https://hifis.net/doc/service-integration/local-agent/) -->
<!--- oidc on the commandline-->
<!--    - [oidc-agent](https://github.com/indigo-dc/oidc-agent)-->
<!--    - [mytoken](https://mytoken.data.kit.edu)-->
<!--- [ssh/oidc](https://github.com/EOSC-synergy/ssh-oidc) with federated identities-->


## Outlook
- Integrate more services
    - Large Resources (HPC / Clusters)
- Spread the technology
- Interoperate with other Community AAIs
    - How to handle cross-community access?
    - How about OIDC-Federations?
- Contribute to AARC Guidelines:
    - IdP Hinting
    - SCIM and Deprovisioning
    - Expression of Entitlements
- Manage expectations, e.g. identity linking

## More information

[https://aai.helmholtz.de](https://aai.helmholtz.de)

